# HID Jog Shuttle

Simple Jog Shuttle for kdenlive video editor based on OBJECTIVE DEVELOPMENT
GmbH's V-USB driver software and HID Keys Example:
https://www.obdev.at/products/vusb/hidkeys.html

DESCRIPTION

The device is assembled on an Atmel Attiny 2313 microcontroller.

Allows you to make fast and accurate cutting of clips in
kdenlive video editor, but can be used with other video editors,
who have the ability to reassign keys to perform certain
action.

Consists of 3 buttons and an encoder.

When the encoder knob is rotated, keystrokes of the keyboard with arrows are
emulated "left" and "right", allowing frame-by-frame to move the pointer to the
current position on timeline. If at the same time hold Shift on the keyboard,
then moving in seconds.

When you press the encoder button, the space bar is emulated, which includes or
stops playback at the current position.

The right and left buttons of the device emulate Home and End, respectively, and
move the current position to the beginning or end of the selected clip. And when
pressed together with Ctrl - move to the beginning or end of the project.

Middle button of the device - emulates pressing Shift + R, cutting the clip into
the selected track at the current position on the timeline.

CIRCUIT

The device circuit is located in the "circuit" directory. There are schematic
files and PCB layout in Autodesk Eagle 9 format.

FIRMWARE

The current firmware (jogshuttle.2020.05.28.hex) is compiled into AVR-GCC 5.4.0.
To download to the microcontroller, I used AVRDUDE and the USBASP programmer.

FUSES

```
High byte:
0xcf = 1 1 0 0 1 1 1 1 <- RSTDISBL
       ^ ^ ^ ^ ^ ^ ^ ^ ------ BODLEVEL0
       | | | | | + -------- BODLEVEL1
       | | | | + --------- BODLEVEL2
       | | | + -------------- WDTON
       | | + ---------------- SPIEN
       | + ------------------ EESAVE
       + -------------------- DWEN
Low byte:
0xef = 1 1 1 0 1 1 1 1
       ^ ^ \ / \ - + - /
       | | | + ------- CKSEL 3..0 (external> 8M crystal)
       | | + --------------- SUT 1..0 (crystal osc)
       | + ------------------ CKOUT
       + -------------------- CKDIV8
```


avrdude -p t2313 -c usbasp -U lfuse:w:0xef:m -U hfuse:w:0xcf:m -U efuse:w:0xff:m

SOURCES

The source code is almost unchanged taken from the HID Keys Example project.
The reading of buttons for working with the encoder and the initialization
procedure on work with attiny 2313.

Located in the source directory. For assembly and program, a Makefile is used.
Challenges:
make - compilation and assembly in HEX.
make clean - delete the results of the previous assembly
make flash - firmware of the FLASH microcontroller
make fuse - FUSES programming

###############################################################################

А теперь то же самое, но по-русски:

ОПИСАНИЕ

Устройство собрано на микроконтроллере Atmel Attiny 2313.

Позволяет позволяет производить быструю и точную нарезку клипов в
видеоредакторе kdenlive, но может быть использовано с другими видеоредакторами,
у которых есть возможность переназначения кнопок для выполнения тех или иных
действий.

Состоит из 3 кнопок и энкодера.

При вращении ручки энкодера эмулируются нажатия кнопок клавиатуры со стрелками
"влево" и "вправо", позволяя по-кадрово перемещать указатель текущей позиции на
шкале времени. А если при этом удерживать нажатым Shift на клавиатуре, то
перемещение осуществляется по-секундно.

При нажатии кнопки энкодера эмулируется нажатие "Пробела", что включает или
останавливает воспроизведение на текущей позиции.

Правая и левая кнопки устройства эмулируют Home и End соответственно, и
перемещают текущую позицию в начало или конец выделенного клипа. А при нажатии
совместно с Ctrl - перемещают в начало или конец проекта.

Средняя кнопка устройства - эмулирует нажатие Shift + R, разрезая клип на
выделенное дорожке в текущей позиции на шкале времени.

СХЕМА

Схема устройства расположена в каталоге circuit. Там же находятся файлы схемы и
разводки печатной платы в формате Autodesk Eagle 9.

ПРОШИВКА

Текущая прошивка (jogshuttle.2020.05.28.hex) скомпилирована в AVR-GCC 5.4.0.
Для загрузки в микроконтроллер я использовал AVRDUDE и программатор USBASP.

FUSES

```
High byte:
0xcf = 1 1 0 0   1 1 1 1 <-- RSTDISBL
       ^ ^ ^ ^   ^ ^ ^------ BODLEVEL0
       | | | |   | +-------- BODLEVEL1
       | | | |   + --------- BODLEVEL2
       | | | +-------------- WDTON
       | | +---------------- SPIEN
       | +------------------ EESAVE
       +-------------------- DWEN
Low byte:
0xef = 1 1 1 0   1 1 1 1
       ^ ^ \ /   \--+--/
       | |  |       +------- CKSEL 3..0 (external >8M crystal)
       | |  +--------------- SUT 1..0 (crystal osc)
       | +------------------ CKOUT
       +-------------------- CKDIV8
```


avrdude -p t2313 -c usbasp -U lfuse:w:0xef:m -U hfuse:w:0xcf:m -U efuse:w:0xff:m

ИСХОДНИКИ

Исходные тексты практически без изменений взяты из проекта HID Keys Example.
Переделано чтение кнопок на работу с энкодером и процедура инициализации на
работу с attiny 2313.

Расположены в каталоге source. Для сборки и прошивки используется Makefile.
Вызовы:
make - компиляция и сборка в HEX.
make clean - удаление результатов предыдущей сборки
make flash - прошивка FLASH микрокнтроллера
make fuse - прграммирование FUSES
